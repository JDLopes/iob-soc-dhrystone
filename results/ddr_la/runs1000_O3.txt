make[1]: Entering directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc'
make -C simulation/icarus 
make[2]: Entering directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc/simulation/icarus'
sed "s/\#/\`/;s/0x/8\'h/" ../../software/ld-sw/console.h | sed "/CONSOLE_H\|endif/d" > console.vh
make -C ../../software/firmware BAUD=30000000 FREQ=100000000
make[3]: Entering directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc/software/firmware'
sed s/\`/\#/g ../../rtl/include/system.vh > system.h
riscv32-unknown-elf-gcc -Os -O3 -ffreestanding -nostdlib -march=rv32im -mabi=ilp32 -Wno-implicit-int -Wno-implicit-function-declaration -o firmware.elf -DUART_BAUD_RATE=30000000 -DUART_CLK_FREQ=100000000 -DRISCV -DTIME -DNUMBER_OF_RUNS=1000 -I. -I../../submodules/iob-uart/c-driver -I../../submodules/iob-cache/c-driver dhry_1.c dhry_2.c stdlib.c syscalls.c ../../submodules/iob-uart/c-driver/iob-uart.c ../../submodules/iob-cache/c-driver/iob-cache.c firmware.S --std=gnu99 -Wl,-Bstatic,-T,dhry.lds,-Map,firmware.map,--strip-debug -lgcc -lc
riscv32-unknown-elf-objcopy -O binary firmware.elf firmware.bin
../python/makehex.py firmware.bin `../bash/get_binsize.sh firmware.bin` > progmem.hex
../python/makehex.py firmware.bin `../python/get_memsize.py MAINRAM_ADDR_W` > firmware.hex
make[3]: Leaving directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc/software/firmware'
cp ../../software/firmware/firmware.hex .
cp ../../software/firmware/firmware.bin .
../../software/python/hex_split.py firmware
make -C ../../software/bootloader BAUD=30000000 FREQ=100000000
make[3]: Entering directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc/software/bootloader'
sed s/\`/\#/g ../../rtl/include/system.vh > system.h
riscv32-unknown-elf-gcc -Os -ffreestanding -nostdlib -march=rv32im -mabi=ilp32 -o boot.elf -I. -I../../submodules/iob-uart/c-driver -I../../submodules/iob-cache/c-driver -I../ld-sw -DUART_BAUD_RATE=30000000 -DUART_CLK_FREQ=100000000 -DPROG_SIZE=`wc -c   ../../software/firmware/firmware.bin | head -n1 | cut -d " " -f1` boot.S boot.c ../../submodules/iob-uart/c-driver/iob-uart.c ../../submodules/iob-cache/c-driver/iob-cache.c  --std=gnu99 -Wl,-Bstatic,-T,boot.lds,-Map,boot.map,--strip-debug -lgcc -lc
riscv32-unknown-elf-objcopy -O binary boot.elf boot.bin
../python/makehex.py boot.bin `../python/get_memsize.py BOOTROM_ADDR_W` > boot.hex
make[3]: Leaving directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc/software/bootloader'
cp ../../software/bootloader/boot.hex .
../../software/python/hex_split.py boot
cp boot.hex boot.dat
iverilog -W all -g2005-sv -DVCD -I. -I../../rtl/include -I../../submodules/iob-uart/rtl/include -I../../submodules/iob-cache/rtl/header -DPROG_SIZE=22048 -DUART_BAUD_RATE=30000000 -DUART_CLK_FREQ=100000000 ../../rtl/testbench/system_tb.v  ../../rtl/src/*.v ../../rtl/src/memory/behav/*.v ../../submodules/iob-rv32/picorv32.v ../../submodules/iob-uart/rtl/src/iob-uart.v ../../submodules/fifo/afifo.v ../../submodules/iob-cache/rtl/src/gen_mem_reg.v ../../submodules/iob-cache/rtl/src/iob-cache.v ../../submodules/axi-mem/rtl/axi_ram.v
./a.out
VCD info: dumpfile system.vcd opened for output.

Dhrystone Benchmark, Version 2.1 (Language: C)

Program compiled without 'register' attribute

Please give the number of runs through the benchmark: 
Execution starts, 1000 runs through Dhrystone
        should be:   Number_Of_Runs + 10
Ptr_Glob->
  Ptr_Comp:          22056
        should be:   (implementation-dependent)
  Discr:             0
        should be:   0
  Enum_Comp:         2
        should be:   2
  Int_Comp:          17
        should be:   17
  Str_Comp:          DHRYSTONE PROGRAM, SOME STRING
        should be:   DHRYSTONE PROGRAM, SOME STRING
Next_Ptr_Glob->
  Ptr_Comp:          22056
        should be:   (implementation-dependent), same as above
  Discr:             0
        should be:   0
  Enum_Comp:         1
        should be:   1
  Int_Comp:          18
        should be:   18
  Str_Comp:          DHRYSTONE PROGRAM, SOME STRING
        should be:   DHRYSTONE PROGRAM, SOME STRING
Int_1_Loc:           5
        should be:   5
Int_2_Loc:           13
        should be:   13
Int_3_Loc:           7
        should be:   7
Enum_Loc:            1
        should be:   1
Str_1_Loc:           DHRYSTONE PROGRAM, 1'ST STRING
        should be:   DHRYSTONE PROGRAM, 1'ST STRING
Str_2_Loc:           DHRYSTONE PROGRAM, 2'ND STRING
        should be:   DHRYSTONE PROGRAM, 2'ND STRING

Number_Of_Runs: 1000
User_Time: 2611154 cycles, 407024 insn
Cycles_Per_Instruction: -4.-1-3-6
Dhrystones_Per_Second_Per_MHz: 382
DMIPS_Per_MHz: 0.217

END OF DHRYSTONE

make[2]: Leaving directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc/simulation/icarus'
make[1]: Leaving directory `/home/asilva/sandbox/iob-soc-dhrystone/submodules/iob-soc'
